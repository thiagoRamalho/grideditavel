package br.com.grideditavel.model;

import java.util.Calendar;

public class Pessoa {

	private String nome;
	private int idade;
	private Calendar dataNascimento;
	private boolean isSelecionado = false;
	private int idadefixa = 5;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public Calendar getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public boolean isSelecionado() {
		return isSelecionado;
	}
	public void setSelecionado(boolean isSelecionado) {
		this.isSelecionado = isSelecionado;
	}
	
	public int getIdadeFixa() {
		return idadefixa;
	}
}
