package br.com.grideditavel.view;

import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

/**
 * Custom table criada apenas para alterar cor da celula
 * sem perder demais formatacoes
 * 
 * @author Thiago Ramalho
 *
 */
class JCustomTable extends JTable{
	
	private Map<Integer, Color> coresColunaEspecifica;
	
	private static final long serialVersionUID = 1L;

	public JCustomTable(TableModel tableModel) {
		super(tableModel);
		this.coresColunaEspecifica = new HashMap<Integer, Color>();
	}

	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component stamp = super.prepareRenderer(renderer, row, column);
		if (this.coresColunaEspecifica.containsKey(column)){

			stamp.setBackground(this.coresColunaEspecifica.get(column));
			stamp.setForeground(this.getForeground());

			if(isCellSelected(row, column)){
				stamp.setBackground(this.getSelectionBackground());
				stamp.setForeground(this.getSelectionForeground());
			}
		}
		return stamp;
	}
	
	public void addCoresColunaEspecifica(int coluna, Color color){
		this.coresColunaEspecifica.put(coluna, color);
	}
	
}