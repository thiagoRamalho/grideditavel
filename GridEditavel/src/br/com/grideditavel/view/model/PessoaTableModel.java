package br.com.grideditavel.view.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import br.com.grideditavel.model.Pessoa;

public class PessoaTableModel extends JCustomTableModel<Pessoa>{

	private static final long serialVersionUID = 1L;
	private SimpleDateFormat format;

	public PessoaTableModel(List<Pessoa> dados, List<String> colunas) {
		super(dados, colunas);
		this.format = new SimpleDateFormat("dd/MM/yyyy");
	}
	
	/**
	 * Implementa o retorno para as coordenadas 
	 * 
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		if(!this.dados.isEmpty()){

			Pessoa p = this.dados.get(rowIndex);

			if(columnIndex == 0){
				return new Boolean(p.isSelecionado());
			}
			else if(columnIndex == 1){
				return p.getNome();
			}
			else if(columnIndex == 2){
				return p.getIdade();
			}
			else if(columnIndex == 3){
				return p.getIdadeFixa();
			}
			else if(columnIndex == 4){
				return this.formatadorData(p.getDataNascimento());
			}
		}
		return "";
	}
	
	/**
	 * Implementa a insercao na celula com base nas coordenadas (row e column)
	 * importante para os eventos de edicao das celulas
	 * 
	 */
	@Override  
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {  

		if(this.isValidRow(rowIndex) && this.isValidColumn(columnIndex)){

			Pessoa p = this.dados.get(rowIndex);
			//insere a informacao editada nas celulas de checkbox ou idade
			if(columnIndex == 0){
				p.setSelecionado((Boolean) aValue);
			}
			else if(columnIndex == 2){
				//somente altera se nao for valor invalido 
				try{
					
					Integer valueOf = Integer.valueOf(aValue.toString());
					
					p.setIdade(valueOf);
					
				}catch(NumberFormatException e){
					//se der exception nao fazemos nada ja que se trata
					//de valores invalidos
				}
			}

			fireTableCellUpdated(rowIndex, rowIndex);
		}  
	}
	
	/**
	 * Determina quais celulas podem ser editaveis
	 * 
	 */
	@Override  
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		boolean isEditable = false;

		if(columnIndex == 0){
			isEditable = true;
		}
		else if(columnIndex == 2){
			//recupera o objeto da linha especificada
			Pessoa p = this.dados.get(rowIndex);
			//retorna se a celula idade sera ou nao editavel
			//verificando se a celula do checkbox esta marcada
			isEditable = p.isSelecionado();
		}
		return  isEditable; 
	}  
	
	/**
	 * Formatador de datas para que as mesmas sejam
	 * exibidas adequadamente no grid
	 * 
	 * @param c
	 * @return
	 */
	private String formatadorData(Calendar c){

		String resultado = "";
		
		try{
			//se a data for invalida setar essa propriedade
			//gera uma excecao e impede que a data seja
			//arredondada
			format.setLenient(false);
			resultado = format.format(c.getTime());

		}catch(Exception e){}

		return resultado;
	}
}
