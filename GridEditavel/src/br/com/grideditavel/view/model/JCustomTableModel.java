package br.com.grideditavel.view.model;

import java.util.Collections;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public abstract class JCustomTableModel<T> extends AbstractTableModel{

	private static final long serialVersionUID = 1L;
	
	protected List<T> dados = Collections.emptyList();
	private List<String> colunas;
	
	public JCustomTableModel(List<T> dados, List<String> colunas) {
		super();
		if(dados != null){
			this.dados = dados;
		}
		this.colunas = colunas;
	}

	/**
	 * Total de linhas, caso vazio seta valor padrão
	 */
	@Override
	public int getRowCount() {
		
		int size = this.dados.size();
		
		return size > 0 ? size : 5;
	}

	/**
	 * Total de colunas, determinado pelos labels do header
	 */
	@Override
	public int getColumnCount() {
		return this.colunas.size();
	}
	
	/**
	 * Validacao para garantir o acesso ao indice valido
	 * da lista de dados
	 * 
	 * @param index
	 * @return
	 */
	protected boolean isValidRow(int index){
		return (index > -1 && index < this.dados.size());
	}
	
	/**
	 * Validacao para garantir o acesso ao indice valido
	 * a lista de headers
	 * @param index
	 * @return
	 */
	protected boolean isValidColumn(int index){
		return (index > -1 && index < this.colunas.size());
	}
	
	/**
	 * Retorna os valores existentes nos headers conforme
	 * o indice enviado
	 * 
	 */
    @Override
    public String getColumnName(int column) {
        return this.isValidColumn(column) ? colunas.get(column) : "";
    }
    
    /**
     * Retorna o tipo do dado de uma determinada celula
     * importante pois no caso de um boolean, ira torna-la
     * um checkbox
     * 
     */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		
		if(this.dados.isEmpty()){
			return String.class;
		}
		
		return this.getValueAt(0, columnIndex).getClass();
	}

	public T getEntidade(int row){
		
		T entidade = null;
		
		if(!this.dados.isEmpty() && this.dados.size() > row  && row > -1){
			entidade = this.dados.get(row);
		}
		
		return entidade;
	}
	
}
