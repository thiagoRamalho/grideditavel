package br.com.grideditavel.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import br.com.grideditavel.model.Pessoa;
import br.com.grideditavel.view.model.PessoaTableModel;

public class DlgGrid extends JFrame {

	private static final int COLUNA_IDADE_EDITAVEL = 2;
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JCustomTable table;
	private ArrayList<Pessoa> listaDePessoas;
	private JLabel lbltotalIdadeSomatoria;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DlgGrid frame = new DlgGrid();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DlgGrid() {

		try{

			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 450, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);

			listaDePessoas = new ArrayList<Pessoa>();
			listaDePessoas.add(this.criarPessoa(10));
			listaDePessoas.add(this.criarPessoa(20));
			listaDePessoas.add(this.criarPessoa(30));
			listaDePessoas.add(this.criarPessoa(40));
			listaDePessoas.add(this.criarPessoa(50));
			listaDePessoas.add(this.criarPessoa(60));

			List<String> cabecalho = Arrays.asList(new String[]{"!","Nome", "Idade", "Idade Fixa", "Data Nascimento"});

			table = new JCustomTable(new PessoaTableModel(listaDePessoas, cabecalho));
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setAlignmentY(Component.TOP_ALIGNMENT);
			table.setAlignmentX(Component.LEFT_ALIGNMENT);

			JScrollPane scrollPane = new JScrollPane(table);
			scrollPane.setBounds(6, 91, 426, 101);
			contentPane.add(scrollPane);

			JLabel lblTotalIdade = new JLabel("Total Idade");
			lblTotalIdade.setBounds(6, 235, 85, 16);
			contentPane.add(lblTotalIdade);

			lbltotalIdadeSomatoria = new JLabel(""+somarIdades());
			lbltotalIdadeSomatoria.setBackground(Color.WHITE);
			lbltotalIdadeSomatoria.setForeground(Color.RED);
			lbltotalIdadeSomatoria.setBounds(103, 235, 61, 16);
			contentPane.add(lbltotalIdadeSomatoria);

			//adiciona um listener de modificacao da tabela
			table.getModel().addTableModelListener(new ListenerTabela(lbltotalIdadeSomatoria));

			//background padrao
			table.setBackground(Color.LIGHT_GRAY);

			JButton btnAcao = new JButton("Acao");
			btnAcao.setBounds(315, 230, 117, 29);
			contentPane.add(btnAcao);

			//altera as cores de background para as colunas checkbox e idade
			table.addCoresColunaEspecifica(0, Color.ORANGE);
			table.addCoresColunaEspecifica(2, Color.ORANGE);

			//** as implementacoes abaixo trabalham em conjunto com o model da tabela **

			//recupera o gerenciador de colunas da tabela
			TableColumn col = table.getColumnModel().getColumn(COLUNA_IDADE_EDITAVEL);
			//cria um novo editor de celulas e utiliza o component criado
			//como container
			DefaultCellEditor cellEditor = new CustomCellEditor(table);
			//reinsere o editor de celulas no gerenciador de colunas
			col.setCellEditor(cellEditor);

			//adiciona a acao ao botao para tambem invocar a validacao
			btnAcao.addActionListener(new AcaoListener(cellEditor));

		}catch(Exception e){
			e.printStackTrace();
		}

	}

	private int somarIdades(){

		int totalIdade = 0;

		for (Pessoa p : this.listaDePessoas) {
			totalIdade += p.getIdade();
		}

		return totalIdade;
	}

	private Pessoa criarPessoa(int random) {

		Pessoa p = new Pessoa();
		p.setDataNascimento(Calendar.getInstance());
		p.setIdade(random);
		p.setNome("nome "+random);

		return p;
	}

	/**
	 * Cria um listener que observa as alteracoes na tabela
	 * @author Thiago Ramalho
	 *
	 */
	class ListenerTabela implements TableModelListener{

		private JLabel label;

		public ListenerTabela(JLabel label) {
			super();
			this.label = label;
		}

		@Override
		public void tableChanged(TableModelEvent e) {
			//atualiza a somatoria das idades
			label.setText(""+somarIdades());
		}
	}
}

/**
 * Editor responsavel por controlar eventos da 
 * celula editada
 * 
 * @author Thiago Ramalho
 *
 */
class CustomCellEditor extends DefaultCellEditor{

	private static final long serialVersionUID = 1L;
	private JTextField field;
	private JCustomTable table;


	public CustomCellEditor(JCustomTable table) {
		super(new JTextField());
		this.table = table;

		//recupera a referencia do textfield enviado no construtor
		field = (JTextField) this.editorComponent;

		//alinhamento e aparencia
		field.setBorder(null);
		field.setHorizontalAlignment(SwingConstants.RIGHT);

		//adiciona evento de focu para que marque o valor
		//da celula
		field.addFocusListener(new Focus());

		//restringe tipos de dados inseridos
		field.setDocument(new NumericOnlyDocument());

		//forca que seja editavel com apenas um click		
		this.setClickCountToStart(1);
	}

	/**
	 * Metodo que verifica se o valor digitado e valido
	 * antes de adiciona-lo ao objeto que representa os dados
	 * 
	 */
	@Override
	public boolean stopCellEditing() {

		PessoaTableModel model = (PessoaTableModel) this.table.getModel();

		Pessoa entidade = model.getEntidade(this.table.getEditingRow());

		if(entidade != null){

			String valor = field.getText();

			System.out.println("Editavel: " + valor + " Fixa: "+entidade.getIdadeFixa());

			if(valor == null || valor.trim().isEmpty()){
				JOptionPane.showMessageDialog(null, "VALOR INVALIDO");
				field.requestFocus();
				return false;
			}
			else if(Integer.valueOf(valor).intValue() < entidade.getIdadeFixa()){
				JOptionPane.showMessageDialog(null, "Idade deve ser maior que idade fixa");
				field.requestFocus();
				return false;
			}
		}
		return super.stopCellEditing();
	}
}

/**
 * Classe responsavel por selecionar o conteudo no momento
 * que a celula editada recebe foco
 * 
 * @author Thiago Ramalho
 *
 */
class Focus extends FocusAdapter {

	@Override
	public void focusGained(FocusEvent e) {
		JTextField component2 = (JTextField) e.getComponent();
		component2.selectAll();
		component2.requestFocus();
		System.out.println("focu");
	}
}

class AcaoListener implements ActionListener{

	private TableCellEditor cellEditor;

	public AcaoListener(TableCellEditor cellEditor) {
		this.cellEditor = cellEditor;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//com o editor invoca seu metodo de finalizacao
		//iniciando as validacoes existentes
		cellEditor.stopCellEditing();
	}
}

class NumericOnlyDocument extends PlainDocument{

	private static final long serialVersionUID = 1L;

	public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {

		try{
			//realiza a validacao pois se nao for numerico ira gerar exception
			Integer.valueOf(str);

			super.insertString(offs, str, a);

		}catch(NumberFormatException e){
			//se cair na exception nao atualiza o valor do campo
		}

	}

}
